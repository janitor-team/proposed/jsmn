Source: jsmn
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13),
Standards-Version: 4.5.1
Section: libs
Homepage: https://github.com/zserge/jsmn
Vcs-Browser: https://salsa.debian.org/science-team/jsmn
Vcs-Git: https://salsa.debian.org/science-team/jsmn.git
Rules-Requires-Root: no

Package: libjsmn-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: header-only JSON library
 Most JSON parsers offer you a bunch of functions to load JSON data,
 parse it and extract any value by its name. jsmn proves that checking
 the correctness of every JSON packet or allocating temporary objects
 to store parsed JSON fields often is an overkill.
 .
 jsmn is designed to be robust (it should work fine even with erroneous
 data), fast (it should parse data on the fly), portable (no superfluous
 dependencies or non-standard C extensions). And of course, simplicity is
 a key feature - simple code style, simple algorithm, simple integration
 into other projects.
 .
 Features
 .
  * compatible with C89
  * no dependencies (even libc!)
  * highly portable (tested on x86/amd64, ARM, AVR)
  * about 200 lines of code
  * extremely small code footprint
  * API contains only 2 functions
  * no dynamic memory allocation
  * incremental single-pass parsing
  * library code is covered with unit-tests
